import { VamoaverPage } from './app.po';

describe('vamoaver App', function() {
  let page: VamoaverPage;

  beforeEach(() => {
    page = new VamoaverPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
